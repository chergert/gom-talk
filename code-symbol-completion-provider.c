#include <glib/gi18n.h>
#include <gom/gom.h>
#include <gtksourceview/gtksourcecompletionitem.h>

#include "code-symbol.h"
#include "code-symbol-completion-provider.h"

struct _CodeSymbolCompletionProviderPrivate
{
   GomRepository *repository;

   GdkPixbuf *pixbuf_method;
   GdkPixbuf *pixbuf_type;
};

enum
{
   PROP_0,
   PROP_REPOSITORY,
   LAST_PROP
};

static void init_provider (GtkSourceCompletionProviderIface *iface);

G_DEFINE_TYPE_EXTENDED (CodeSymbolCompletionProvider,
                        code_symbol_completion_provider,
                        G_TYPE_OBJECT,
                        0,
                        G_IMPLEMENT_INTERFACE (GTK_SOURCE_TYPE_COMPLETION_PROVIDER, init_provider)
                        G_ADD_PRIVATE (CodeSymbolCompletionProvider))

static GParamSpec *gParamSpecs [LAST_PROP];

static void
code_symbol_completion_provider_finalize (GObject *object)
{
   CodeSymbolCompletionProviderPrivate *priv;

   priv = CODE_SYMBOL_COMPLETION_PROVIDER (object)->priv;

   g_clear_object (&priv->repository);
   g_clear_object (&priv->pixbuf_method);
   g_clear_object (&priv->pixbuf_type);

   G_OBJECT_CLASS (code_symbol_completion_provider_parent_class)->finalize (object);
}

static void
code_symbol_completion_provider_get_property (GObject    *object,
                                              guint       prop_id,
                                              GValue     *value,
                                              GParamSpec *pspec)
{
   CodeSymbolCompletionProvider *provider = CODE_SYMBOL_COMPLETION_PROVIDER (object);

   switch (prop_id) {
   case PROP_REPOSITORY:
      g_value_set_object (value, provider->priv->repository);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
   }
}

static void
code_symbol_completion_provider_set_property (GObject      *object,
                                              guint         prop_id,
                                              const GValue *value,
                                              GParamSpec   *pspec)
{
   CodeSymbolCompletionProvider *provider = CODE_SYMBOL_COMPLETION_PROVIDER (object);

   switch (prop_id) {
   case PROP_REPOSITORY:
      g_clear_object (&provider->priv->repository);
      provider->priv->repository = g_value_dup_object (value);
      g_object_notify_by_pspec (object, pspec);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
   }
}

static void
code_symbol_completion_provider_class_init (CodeSymbolCompletionProviderClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS (klass);
   object_class->finalize = code_symbol_completion_provider_finalize;
   object_class->get_property = code_symbol_completion_provider_get_property;
   object_class->set_property = code_symbol_completion_provider_set_property;

   gParamSpecs [PROP_REPOSITORY] =
      g_param_spec_object ("repository",
                           _("Repository"),
                           _("The GOM data repository."),
                           GOM_TYPE_REPOSITORY,
                           (G_PARAM_READWRITE |
                            G_PARAM_STATIC_STRINGS));
   g_object_class_install_property (object_class, PROP_REPOSITORY,
                                    gParamSpecs [PROP_REPOSITORY]);
}

static void
code_symbol_completion_provider_init (CodeSymbolCompletionProvider *provider)
{
   provider->priv = code_symbol_completion_provider_get_instance_private (provider);

   provider->priv->pixbuf_method = gdk_pixbuf_new_from_file ("method-16x.png", NULL);
   provider->priv->pixbuf_type = gdk_pixbuf_new_from_file ("class-16x.png", NULL);
}

static gchar *
provider_get_name (GtkSourceCompletionProvider *provider)
{
   return g_strdup(_("CodeSymbol"));
}

static gboolean
is_stop_char (gunichar c)
{
   switch (c) {
   case '_':
      return FALSE;
   case ')':
   case '(':
   case '&':
   case '*':
   case '{':
   case '}':
   case ' ':
   case '\t':
   case '[':
   case ']':
   case '=':
   case '"':
   case '\'':
      return TRUE;
   default:
      return !g_unichar_isalnum(c);
   }
}

static gchar *
get_word (GtkSourceCompletionProvider *provider,
          GtkTextIter                 *iter)
{
   GtkTextIter *end;
   gboolean moved = FALSE;
   gunichar c;
   gchar *word;

   end = gtk_text_iter_copy(iter);

   do {
      if (!gtk_text_iter_backward_char(iter)) {
         break;
      }
      c = gtk_text_iter_get_char(iter);
      moved = TRUE;
   } while (!is_stop_char(c));

   if (moved && !gtk_text_iter_is_start(iter)) {
      gtk_text_iter_forward_char(iter);
   }

   word = g_strstrip(gtk_text_iter_get_text(iter, end));

   gtk_text_iter_free(end);

   return word;
}

#define MAX_ITEMS 1000

static GtkSourceCompletionItem *
create_item (CodeSymbolCompletionProvider *self,
             CodeSymbol *symbol)
{
   GdkPixbuf *pixbuf = NULL;

   switch (code_symbol_get_symbol_type (symbol)) {
   case CODE_SYMBOL_TYPE:
      pixbuf = self->priv->pixbuf_type;
      break;
   case CODE_SYMBOL_FUNCTION:
      pixbuf = self->priv->pixbuf_method;
      break;
   default:
      break;
   }

   return gtk_source_completion_item_new (
      code_symbol_get_name (symbol),
      code_symbol_get_name (symbol),
      pixbuf,
      NULL);
}

static void
on_fetch_cb (GObject      *source,
             GAsyncResult *result,
             gpointer      user_data)
{
   GomResourceGroup *group = (GomResourceGroup *)source;
   gpointer *state = user_data;
   GtkSourceCompletionProvider *provider = state[0];
   GtkSourceCompletionContext *context = state[1];
   GtkSourceCompletionItem *item;
   GomResource *resource;
   GList *list = NULL;
   gint i;

   for (i = 0; i < MAX_ITEMS; i++) {
      resource = gom_resource_group_get_index (group, i);
      if (!resource) {
         break;
      }

      item = create_item (CODE_SYMBOL_COMPLETION_PROVIDER (provider),
                          CODE_SYMBOL (resource));
      list = g_list_prepend (list, item);
   }

   list = g_list_reverse (list);

   gtk_source_completion_context_add_proposals (context, provider, list, TRUE);

   g_list_foreach (list, (GFunc)g_object_unref, NULL);
   g_list_free (list);
}

static void
on_find_cb (GObject      *source,
            GAsyncResult *result,
            gpointer      user_data)
{
   GomRepository *repository = (GomRepository *)source;
   gpointer *state = user_data;
   GtkSourceCompletionProvider *provider = state[0];
   GtkSourceCompletionContext *context = state[1];
   GomResourceGroup *group;
   GError *error = NULL;

   group = gom_repository_find_finish (repository, result, &error);

   if (!group) {
      g_printerr ("%s\n", error->message);
      g_error_free (error);
      gtk_source_completion_context_add_proposals (context, provider, NULL, TRUE);
      g_object_unref (provider);
      g_object_unref (context);
      g_free (state);
      return;
   }

   gom_resource_group_fetch_async (group,
                                   0,
                                   MAX_ITEMS,
                                   on_fetch_cb,
                                   state);

   g_object_unref (group);
}

static gchar *
to_like (gchar *foo)
{
   gchar *ret = g_strdup_printf ("%s%%", foo);
   g_free (foo);
   return ret;
}

static void
provider_populate (GtkSourceCompletionProvider *provider,
                   GtkSourceCompletionContext  *context)
{
   CodeSymbolCompletionProvider *p = CODE_SYMBOL_COMPLETION_PROVIDER(provider);
   GomFilter *filter = NULL;
   gpointer *state;
   GtkTextIter iter;
   GValue val = { 0 };
   gchar *word;

   if (p->priv->repository) {
      gtk_source_completion_context_get_iter (context, &iter);

      word = g_strstrip (get_word (provider, &iter));
      if (!word || !*word) {
         gtk_source_completion_context_add_proposals (context, provider, NULL, TRUE);
         g_free (word);
         return;
      }

      state = g_new0 (gpointer, 2);
      state[0] = g_object_ref (provider);
      state[1] = g_object_ref (context);

      word = to_like (word);
      g_value_init (&val, G_TYPE_STRING);
      g_value_take_string (&val, word);
      filter = gom_filter_new_like (CODE_TYPE_SYMBOL, "name", &val);
      g_value_unset (&val);

      gom_repository_find_async (p->priv->repository,
                                 CODE_TYPE_SYMBOL,
                                 filter,
                                 on_find_cb,
                                 state);

      g_object_unref (filter);
   }
}

static void
init_provider (GtkSourceCompletionProviderIface *iface)
{
   iface->get_name = provider_get_name;
   iface->populate = provider_populate;
#if 0
   iface->activate_proposal = provider_activate_proposal;
   iface->get_interactive_delay = provider_get_interactive_delay;
   iface->get_priority = provider_get_priority;
#endif
}
