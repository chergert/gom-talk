all: code

PKGS = gom-1.0 gtksourceview-3.0 gtk+-3.0
FILES = \
	code-symbol.c \
	code-symbol.h \
	code-symbol-completion-provider.c \
	code-symbol-completion-provider.h \
	main.c

code: $(FILES)
	$(CC) -o $@ -Wall -Werror -ggdb $(shell pkg-config --cflags --libs $(PKGS)) $(FILES)

clean:
	rm -rf code
