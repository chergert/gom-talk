#include <glib/gi18n.h>

#include "code-symbol.h"

struct _CodeSymbolPrivate
{
   gint id;
   CodeSymbolType symbol_type;
   gchar *name;
};

enum
{
   PROP_0,
   PROP_ID,
   PROP_NAME,
   PROP_SYMBOL_TYPE,
   LAST_PROP
};

G_DEFINE_TYPE_WITH_PRIVATE (CodeSymbol, code_symbol, GOM_TYPE_RESOURCE)

static GParamSpec *gParamSpecs [LAST_PROP];

gint
code_symbol_get_id (CodeSymbol *symbol)
{
   return symbol->priv->id;
}

void
code_symbol_set_id (CodeSymbol *symbol,
                    gint        id)
{
   symbol->priv->id = id;
   g_object_notify_by_pspec (G_OBJECT (symbol), gParamSpecs [PROP_ID]);
}

const char *
code_symbol_get_name (CodeSymbol *symbol)
{
   return symbol->priv->name;
}

void
code_symbol_set_name (CodeSymbol  *symbol,
                      const gchar *name)
{
   g_free (symbol->priv->name);
   symbol->priv->name = g_strdup (name);
   g_object_notify_by_pspec (G_OBJECT (symbol), gParamSpecs [PROP_NAME]);
}

CodeSymbolType
code_symbol_get_symbol_type (CodeSymbol *symbol)
{
   return symbol->priv->symbol_type;
}

void
code_symbol_set_symbol_type (CodeSymbol     *symbol,
                             CodeSymbolType  symbol_type)
{
   symbol->priv->symbol_type = symbol_type;
   g_object_notify_by_pspec (G_OBJECT (symbol), gParamSpecs [PROP_SYMBOL_TYPE]);
}

static void
code_symbol_finalize (GObject *object)
{
   CodeSymbolPrivate *priv;

   priv = CODE_SYMBOL (object)->priv;

   g_free (priv->name);

   G_OBJECT_CLASS (code_symbol_parent_class)->finalize (object);
}

static void
code_symbol_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
   CodeSymbol *symbol = CODE_SYMBOL (object);

   switch (prop_id) {
   case PROP_ID:
      g_value_set_int (value, code_symbol_get_id (symbol));
      break;
   case PROP_NAME:
      g_value_set_string (value, code_symbol_get_name (symbol));
      break;
   case PROP_SYMBOL_TYPE:
      g_value_set_enum (value, code_symbol_get_symbol_type (symbol));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
   }
}

static void
code_symbol_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
   CodeSymbol *symbol = CODE_SYMBOL (object);

   switch (prop_id) {
   case PROP_ID:
      code_symbol_set_id (symbol, g_value_get_int (value));
      break;
   case PROP_NAME:
      code_symbol_set_name (symbol, g_value_get_string (value));
      break;
   case PROP_SYMBOL_TYPE:
      code_symbol_set_symbol_type (symbol, g_value_get_enum (value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
   }
}

static void
code_symbol_class_init (CodeSymbolClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;

   object_class = G_OBJECT_CLASS (klass);
   object_class->finalize = code_symbol_finalize;
   object_class->get_property = code_symbol_get_property;
   object_class->set_property = code_symbol_set_property;

   gParamSpecs [PROP_ID] =
      g_param_spec_int ("id",
                        _("Name"),
                        _("Name"),
                        0,
                        G_MAXINT,
                        0,
                        (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
   g_object_class_install_property (object_class, PROP_ID,
                                    gParamSpecs [PROP_ID]);

   gParamSpecs [PROP_NAME] =
      g_param_spec_string ("name",
                           _("Name"),
                           _("The name of the symbol."),
                           NULL,
                           (G_PARAM_READWRITE |
                            G_PARAM_STATIC_STRINGS));
   g_object_class_install_property (object_class, PROP_NAME,
                                    gParamSpecs [PROP_NAME]);

   gParamSpecs [PROP_SYMBOL_TYPE] =
      g_param_spec_enum ("symbol-type",
                         _("Symbol Type"),
                         _("The type of C symbol."),
                         CODE_TYPE_SYMBOL_TYPE,
                         CODE_SYMBOL_FUNCTION,
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));
   g_object_class_install_property (object_class, PROP_SYMBOL_TYPE,
                                    gParamSpecs [PROP_SYMBOL_TYPE]);

   resource_class = GOM_RESOURCE_CLASS (klass);
   gom_resource_class_set_table (resource_class, "symbols");
   gom_resource_class_set_primary_key (resource_class, "id");
}

static void
code_symbol_init (CodeSymbol *symbol)
{
   symbol->priv = code_symbol_get_instance_private (symbol);
}

GType
code_symbol_type_get_type (void)
{
   static const GEnumValue values[] = {
      { CODE_SYMBOL_FUNCTION, "CODE_SYMBOL_FUNCTION", "FUNCTION" },
      { CODE_SYMBOL_TYPE, "CODE_SYMBOL_TYPE", "TYPE" },
      { CODE_SYMBOL_MACRO, "CODE_SYMBOL_MACRO", "MACRO" },
      { 0 }
   };
   static GType type_id;
   static gsize initialized;

   if (g_once_init_enter (&initialized)) {
      type_id = g_enum_register_static ("CodeSymbolType", values);
      g_once_init_leave (&initialized, TRUE);
   }

   return type_id;
}
