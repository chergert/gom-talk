#ifndef CODE_SYMBOL_H
#define CODE_SYMBOL_H

#include <gom/gom.h>

G_BEGIN_DECLS

#define CODE_TYPE_SYMBOL_TYPE       (code_symbol_type_get_type())
#define CODE_TYPE_SYMBOL            (code_symbol_get_type())
#define CODE_SYMBOL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CODE_TYPE_SYMBOL, CodeSymbol))
#define CODE_SYMBOL_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CODE_TYPE_SYMBOL, CodeSymbol const))
#define CODE_SYMBOL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CODE_TYPE_SYMBOL, CodeSymbolClass))
#define CODE_IS_SYMBOL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CODE_TYPE_SYMBOL))
#define CODE_IS_SYMBOL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CODE_TYPE_SYMBOL))
#define CODE_SYMBOL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CODE_TYPE_SYMBOL, CodeSymbolClass))

typedef struct _CodeSymbol        CodeSymbol;
typedef struct _CodeSymbolClass   CodeSymbolClass;
typedef struct _CodeSymbolPrivate CodeSymbolPrivate;

typedef enum _CodeSymbolType
{
   CODE_SYMBOL_FUNCTION,
   CODE_SYMBOL_MACRO,
   CODE_SYMBOL_TYPE,
} CodeSymbolType;

struct _CodeSymbol
{
   GomResource parent;

   /*< private >*/
   CodeSymbolPrivate *priv;
};

struct _CodeSymbolClass
{
   GomResourceClass parent_class;
};

GType           code_symbol_get_type        (void) G_GNUC_CONST;
GType           code_symbol_type_get_type   (void) G_GNUC_CONST;
gint            code_symbol_get_id          (CodeSymbol     *symbol);
void            code_symbol_set_id          (CodeSymbol     *symbol,
                                             gint            id);
const gchar    *code_symbol_get_name        (CodeSymbol     *symbol);
void            code_symbol_set_name        (CodeSymbol     *symbol,
                                             const gchar    *name);
CodeSymbolType  code_symbol_get_symbol_type (CodeSymbol     *symbol);
void            code_symbol_set_symbol_type (CodeSymbol     *symbol,
                                             CodeSymbolType  symbol_type);

G_END_DECLS

#endif /* CODE_SYMBOL_H */
