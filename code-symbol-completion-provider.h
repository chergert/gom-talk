#ifndef CODE_SYMBOL_COMPLETION_PROVIDER_H
#define CODE_SYMBOL_COMPLETION_PROVIDER_H

#include <gtksourceview/gtksourcecompletionprovider.h>

G_BEGIN_DECLS

#define CODE_TYPE_SYMBOL_COMPLETION_PROVIDER            (code_symbol_completion_provider_get_type())
#define CODE_SYMBOL_COMPLETION_PROVIDER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CODE_TYPE_SYMBOL_COMPLETION_PROVIDER, CodeSymbolCompletionProvider))
#define CODE_SYMBOL_COMPLETION_PROVIDER_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CODE_TYPE_SYMBOL_COMPLETION_PROVIDER, CodeSymbolCompletionProvider const))
#define CODE_SYMBOL_COMPLETION_PROVIDER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CODE_TYPE_SYMBOL_COMPLETION_PROVIDER, CodeSymbolCompletionProviderClass))
#define CODE_IS_SYMBOL_COMPLETION_PROVIDER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CODE_TYPE_SYMBOL_COMPLETION_PROVIDER))
#define CODE_IS_SYMBOL_COMPLETION_PROVIDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CODE_TYPE_SYMBOL_COMPLETION_PROVIDER))
#define CODE_SYMBOL_COMPLETION_PROVIDER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CODE_TYPE_SYMBOL_COMPLETION_PROVIDER, CodeSymbolCompletionProviderClass))

typedef struct _CodeSymbolCompletionProvider        CodeSymbolCompletionProvider;
typedef struct _CodeSymbolCompletionProviderClass   CodeSymbolCompletionProviderClass;
typedef struct _CodeSymbolCompletionProviderPrivate CodeSymbolCompletionProviderPrivate;

struct _CodeSymbolCompletionProvider
{
   GObject parent;

   /*< private >*/
   CodeSymbolCompletionProviderPrivate *priv;
};

struct _CodeSymbolCompletionProviderClass
{
   GObjectClass parent_class;
};

GType code_symbol_completion_provider_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CODE_SYMBOL_COMPLETION_PROVIDER_H */
