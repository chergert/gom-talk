#include <gom/gom.h>
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <stdlib.h>

#include "code-symbol.h"
#include "code-symbol-completion-provider.h"

static GomRepository *
load_repository (void)
{
   GomRepository *repository;
   GomAdapter *adapter;
   GList *types;
   GError *error = NULL;

   adapter = gom_adapter_new ();

   if (!gom_adapter_open_sync (adapter, "symbols.db", &error)) {
      g_error ("%s", error->message);
   }

   repository = gom_repository_new (adapter);

   types = g_list_append (NULL, GINT_TO_POINTER (CODE_TYPE_SYMBOL));
   if (!gom_repository_automatic_migrate_sync (repository, 1, types, &error)) {
      g_error ("%s", error->message);
   }

   g_object_unref (adapter);

   return repository;
}

gint
main (int argc,
      char *argv[])
{
   PangoFontDescription *font;
   GtkSourceCompletion *completion;
   GtkSourceCompletionProvider *provider;
   GomRepository *repository;
   GtkWindow *window;
   GtkWidget *scroller;
   GtkWidget *view;

   gtk_init (&argc, &argv);

   repository = load_repository ();

   window = g_object_new (GTK_TYPE_WINDOW,
                          "title", "Code Completion Demo",
                          "default-width", 800,
                          "default-height", 600,
                          "window-position", GTK_WIN_POS_CENTER,
                          NULL);
   g_signal_connect (window, "delete-event", gtk_main_quit, NULL);

   scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                            "visible", TRUE,
                            NULL);
   gtk_container_add (GTK_CONTAINER (window), scroller);

   view = g_object_new (GTK_SOURCE_TYPE_VIEW,
                        "auto-indent", TRUE,
                        "indent-width", 3,
                        "insert-spaces-instead-of-tabs", TRUE,
                        "right-margin-position", 80,
                        "show-line-numbers", TRUE,
                        "show-right-margin", TRUE,
                        "smart-home-end", TRUE,
                        "tab-width", 3,
                        "visible", TRUE,
                        NULL);
   gtk_container_add (GTK_CONTAINER (scroller), view);

   font = pango_font_description_from_string ("Monospace 11");
   gtk_widget_override_font (view, font);
   pango_font_description_free (font);

   completion = gtk_source_view_get_completion (GTK_SOURCE_VIEW (view));
   g_object_set (completion, "show-headers", FALSE, NULL);
   provider = g_object_new (CODE_TYPE_SYMBOL_COMPLETION_PROVIDER,
                            "repository", repository,
                            NULL);
   gtk_source_completion_add_provider (completion, provider, NULL);

   gtk_window_present (window);
   gtk_main ();

   return EXIT_SUCCESS;
}
